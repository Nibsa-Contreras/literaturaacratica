//Importar las bibliotecas
var express 	= require("express");
var http 		= require('http').Server(app);
var io 			= require('socket.io')(http);
var app 		= express();
var fs 			= require('fs');
//Variables
var port 		= 3000;
var app 		= express();
//Asignar la dirección de nuestros recursos públicos
app.use(express.static(__dirname + "/public"));
//Asignar ejs como nuestro motor visual
app.set("view engine", "ejs");

var libros =
[
	{"titulo": "Donde surgen las sombras", "editorial": "prisma de cristal", "autor":"David Lozano Garbala","Paginas": 224, "reseña": "muy bueno"}
]

//invocar json
//app.locals.arreglo = require('./arreglo.json');

//Cargar cada una de las páginas
app.get("/", function(req, res)
{
	res.render("pages/index");
});
app.get("/about", function(req, res)
{
	res.render("pages/about");
});
app.get("/registro", function(req, res)
{
	res.render("pages/registro");
});
app.get("/entrar", function(req, res)
{
	res.render("pages/entrar");
});

//prueba variables json
app.get("/solicitar", function(req, res)
{
	res.render("pages/solicitar");
});

//Montar el server
app.listen(port);
//Mostrar mensaje
console.log("Aplicación arriba en http://localhost:" + port); 